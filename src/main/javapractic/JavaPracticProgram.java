import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class JavaPracticProgram {


    private static void reversString(String string) {
        char[] array = string.toCharArray();
        int size = array.length - 1;
        for (int i = 0; i <= size; size--) {
            System.out.print(array[size]);
        }
    }

    private static void findDuplicateChar(String string) {
        char[] array = string.toCharArray();
        Map<Character, Integer> map = new HashMap();
        int counter = 1;
        for (int i = 0; i < array.length; i++) {

            if (map.containsKey(array[i])) {
                map.put(array[i], counter + 1);
                counter++;
            } else {
                map.put(array[i], 1);
            }
        }
        System.out.println(map.entrySet());
        Set<Character> keys = map.keySet();

        for (char c : keys)

            System.out.println(c + " " + map.get(c));


    }

    private static void duplicateChar(String string) {
        HashMap<Character, Integer> map = new HashMap();
        char[] array = string.toCharArray();
        for (char c : array) {
            if (map.containsKey(c)) {
                map.put(c, map.get(c) + 1);
            } else {
                map.put(c, 1);
            }
        }
        Set<Map.Entry<Character, Integer>> entry = map.entrySet();
        System.out.println(entry);
    }

    private static void armStrom(int number) {
        int arm, tem, c = 0;
        tem = number;
        while (tem > 0) {
            arm = tem % 10;
            c = c + (arm * arm * arm);
            tem = tem / 10;
        }
        if (c==number)
        System.out.println(number+" => is Armstrong number");
        else System.out.println(number+" => is NOT Armstrong number");
    }

    public static void main(String args[]) {
        armStrom(375);
        duplicateChar("rizwanahmad");
    }
}
