package stepdef;

import com.aventstack.extentreports.GherkinKeyword;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;


//TODO: changed ClassMain from cucumber.api.cli.Main To=> cucumber-picocontainer
public class MyStepdefs  {

    @Given("^user is on Home Page$")
    public void user_is_on_Home_Page() {

    }

    @When("^he search for \"([^\"]*)\"$")
    public void he_search_for(String arg1) {

    }

    @When("^choose to buy the first item$")
    public void choose_to_buy_the_first_item() {

    }

    @When("^moves to checkout from mini cart$")
    public void moves_to_checkout_from_mini_cart() {

    }

    @When("^\"([^\"]*)\" sign in on application$")
    public void sign_in_on_application(String arg1) throws Throwable {

    }

    @When("^user select new delivery address$")
    public void user_select_new_delivery_address() {

    }

    @When("^enter \"([^\"]*)\" personal details on checkout page$")
    public void enter_personal_details_on_checkout_page(String arg1) {

    }

    @When("^select payment method as \"([^\"]*)\" payment$")
    public void select_payment_method_as_payment(String arg1) {

    }

    @When("^place the order$")
    public void place_the_order() {

    }

    @Given("I am on login page")
    public void iAmOnLoginPage() throws ClassNotFoundException {

//        System.out.println("I am on login page");
    }


    @When("I click on login button")
    public void iClickOnLoginButton() throws ClassNotFoundException {
               System.out.println("I click on login button");

    }



    @Then("I expect to view profile page")
    public void iExpectToViewProfilePage() throws ClassNotFoundException {
              System.out.println("I expect to view profile page");
    }

    @Given("launch the browser")
    public void launchTheBrowser() throws ClassNotFoundException {

        System.out.println("Browser is launched");
    }

    @And("navigate to the github login page")
    public void navigateToTheGithubLoginPage() throws ClassNotFoundException {

//        System.out.println(" Navigated to login page");
    }

    @And("I enter user id <user id> and password <password>")
    public void iEnterUserIdUserIdAndPasswordPassword() throws ClassNotFoundException {

    }

    @Then("I expect to view my image on top rite")
    public void iExpectToViewMyImageOnTopRite() throws ClassNotFoundException {

        Assert.assertTrue(false);
    }

}
