Feature: Git login functionality

  Background:
    Given launch the browser
    And navigate to the github login page

  Scenario: login with valid credentials
    Given I am on login page
    And I enter user id <user id> and password <password>
    When I click on login button
    Then I expect to view profile page

  Scenario: login with valid credentials
    Given I am on login page
    And I enter user id <user id> and password <password>
    And I click on login button
    When I expect to view profile page
    Then I expect to view my image on top rite

  Scenario Outline: Customer place an order by purchasing an item from search
    Given user is on Home Page
    When he search for "dress"
    And choose to buy the first item
    And moves to checkout from mini cart
    And "<customer>" sign in on application
    And user select new delivery address
    And enter "<customer>" personal details on checkout page
    And select payment method as "check" payment
    And place the order

    Examples:
      | customer |
      | Brian    |